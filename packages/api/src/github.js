import { fetchGet } from '@sample/net';

export const userInfo = async (username) => {
  const apiUrl = `https://api.github.com/users/${username}`;
  const info = await fetchGet(apiUrl);
  return info;
};
