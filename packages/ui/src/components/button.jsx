import { noop } from 'lodash';
import React from 'react';

function LogButton({ onClick = noop }) {
  return (
    <button onClick={onClick} className="bg-sky-600 hover:bg-sky-700">Log</button>
  );
}

export default LogButton;
