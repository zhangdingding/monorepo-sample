const path = require("path");
const esbuild = require('esbuild');

esbuild
  .build({
    entryPoints: [
      path.resolve(__dirname, "./src/index.tsx"),
    ],
    outdir: path.resolve(__dirname, "./dist"),
    bundle: true,
    sourcemap: false,
    minify: true,
    splitting: true,
    format: 'esm',
    target: ['es6']
  })
  .catch(() => process.exit(1));
