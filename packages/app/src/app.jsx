import React from 'react';

import WelcomePage from './pages/welcome';

function App() {
  return (
    <div>
      <WelcomePage />
    </div>
  );
}

export default App;
