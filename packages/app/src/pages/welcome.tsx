import React, { useState, useEffect, useCallback } from 'react';
import { log } from '@sample/shared';
import { LogButton } from '@sample/ui';
import { userInfo as fetchUserInfo } from '@sample/api';

type UserInfo = {
  id: string | number;
  name: string;
  created_at: string;
  url: string;
  followers: number;
  following: number;
};

function App() {
  const [isLoading, setIsLoading] = useState(true);
  const [userName, setUserName] = useState('');

  useEffect(async () => {
    const info: any = await fetchUserInfo('zhangdingding');
    const name = info?.name || '';
    setUserName(name);
    setIsLoading(false);
  }, []);

  const handleClick = useCallback(() => {
    log(`Welcome, ${userName}`);
  }, [userName]);

  return (
    <div>
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        <div>
          <LogButton onClick={handleClick} />
        </div>
      )}
    </div>
  );
}

export default App;
