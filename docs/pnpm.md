# PNPM

[PNPM](https://pnpm.io) 意味着 Performant NPM，是一款快速的、节省磁盘空间的包管理工具。

## Why PNPM?

### 节省空间

当使用 NPM 或 Yarn 时，如果有 100 个项目使用了某个依赖，很可能就会有 100 份该依赖的副本保存在磁盘上。而在使用 NPM 时，依赖会被统一存储在一个**内容可寻址的全局存储库**中，所以：

1. 所有文件（每个包的每个文件）都会统一存储在硬盘上的某一位置。当软件包被被安装时，包里的文件会**硬链接**到这一位置，而不会占用额外的磁盘空间。这允许你跨项目地共享同一版本的依赖。这样就最大化的节省了磁盘空间，项目依赖项越多，这个优势就更明显，并且安装速度要快得多！

2. 如果你用到了某依赖项的不同版本，只会将不同版本间有差异的文件添加到仓库。例如，如果某个包有 100 个文件，而它的新版本只改变了其中 1 个文件。那么 pnpm update 时只会向存储中心额外添加 1 个新文件，而不会因为仅仅一个文件的改变复制整个新版本包的内容。

> pnpm store 中的文件内容是所有（使用 PNPM 安装的）node_modules 文件中唯一“真实”的文件内容。

> 全局存储库的位置可通过 `pnpm store path` 这个命令查到。

### 避免幽灵依赖和依赖分身

在 NPM 3 之前，NPM 安装依赖时是按照依赖树的结构来组织的，但是这样会占据大量的磁盘空间，且在 Windows 下还会造成依赖路径过深的问题。从 NPM 3 开始（包括 Yarn 1），安装依赖项时，所有共用的包都会被提升到 node_modules 的根目录（也就是说 node_modules 目录是扁平的而不是嵌套的），这就意味着，即便项目没有在 package.json 中声明依赖某个依赖，但依旧可以在项目文件中访问到该依赖，这就是所谓的“幽灵依赖 Phantom Dependencies”，如果哪天依赖树的结构发生了改变导致幽灵依赖消失了，但是我们在代码中依然引用了这个依赖，那么代码就会报错。

而 PNPM 通过使用**符号链接**解决了这个问题，即安装依赖时会将依赖项安装到一个“虚拟存储目录”下（这个目录的结构是扁平的），然后将项目依赖链接到项目的虚拟存储目录，这样一来，PNPM 既避免了磁盘占用过大、依赖路径过深的问题，又保持了项目依赖的层级结构，防止幽灵依赖的出现。

同时，PNPM 采用虚拟存储目录的方式还能避免所谓的“依赖分身 NPM Doppelgangers”问题，依赖分身会导致某些包被重复安装，磁盘占用过大的问题。关于什么是依赖分身，可以查阅[这篇文章](https://zhuanlan.zhihu.com/p/404784010)，这里就不赘述了。

> “符号链接”即通常所说的“软链接”。

> “虚拟存储目录”的名称叫做 `.pnpm`，位于项目的顶层 node_modules 下。

![PNPM 非扁平化的 node_modules 文件夹](./assets/node-modules-structure.jpg)

### 快速

平均而言，实际使用中 PNPM 会比 NPM/Yarn 快 2 倍（当然，这个也取决于其他包管理工具的版本，毕竟其他工具也在进化）。这是可以理解的，毕竟 PNPM 的依赖结构设计使得大量的包可以被直接复用，并且首次安装时依赖树的解析也会更简单。

![PNPM Benchmarks](./assets/pnpm-benchmarks-20220508.svg)

> 截止 2022-05-11，PNPM 7.0.1 对比 Yarn 3.2.0 在安装速度上优势不明显，但依然比 NPM 8.9.0 快得多。

### 原生支持 monorepo

PNPM 原生支持 monorepo，不仅通过 workspace 提供了多个 package 之间的管理能力，还通过 filter 提供了对构建任务的编排能力。比如说某个代码库有着四个 package，如果管理工具没有编排能力，那么就需要安装所有 package 的 node_modules。但如果有了编排能力，则可以指定安装某些必需的 package。

加之 PNPM 的 filter 功能丰富、使用简单，可对 package 进行各种组合构建。这些看似无足轻重，但在复杂环境下却可以发挥极大的作用（例如可以提升 CI/CD 的运行效率，降低 CI/CD 脚本的编写难度和维护难度）。

## Reference

- [PNPM 项目初衷](https://pnpm.io/zh/motivation)
- [npm 依赖管理中被忽略的那些细节](https://www.zoo.team/article/npm-details)
- [平铺的结构不是 node_modules 的唯一实现方式](https://pnpm.io/zh/blog/2020/05/27/flat-node-modules-is-not-the-only-way)
- [Pnpm: 最先进的包管理工具](https://zhuanlan.zhihu.com/p/404784010)
- [Why should we use pnpm?](https://www.kochan.io/nodejs/why-should-we-use-pnpm.html)
- [从 Turborepo 看 Monorepo 工具的任务编排能力](https://github.com/worldzhao/blog/issues/13)
