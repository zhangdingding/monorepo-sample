# 单独部署前端 Monorepo 子项目的可行性调研

Monorepo 的优势不必多说，但采用 Monorepo 会带来一个问题，那就是单个代码库的膨胀。有时候我们只需要运行其中一个子项目，但却不得不下载整个代码库，这种浪费在 CI/CD 过程中尤其突出。那么有没有办法解决这个问题呢？答案是有的。

> 本文假定该 Monorepo 使用 Git 做为 Version Control System，且 Git Version >= 2.25.0。

> 本文假定该 Monorepo 使用 TypeScript/JavaScript 技术栈，且使用 PNPM 7.x 做为 Package Manager（PNPM 6.x 应该也可以，但是没有实际测试过）。

## 如何仅下载部分代码，而不下载整个代码库？

这里主要是利用 Git 的三个特性：

1. 部分克隆 (partial clone)
2. 浅克隆 (shallow clone)
3. 稀疏检出 (sparse checkout)

```bash
# 使用 --filter=blob:none --no-checkout 部分 clone 代码库（只 clone 文件组织方式，但不会 clone 文件内容）
# 使用 --depth 1 忽略提交历史及分支关系记录（因为我们只是用来部署程序，只需要最新的代码，无需知道其他历史记录）
git clone --filter=blob:none --no-checkout --depth 1 https://gitlab.com/zhangdingding/monorepo-sample.git

cd monorepo-sample/

# 启用 sparse checkout
git config core.sparseCheckout true
# 使用 cone 模式初始化仓库（仅下载根目录的文件，不下载根目录下的目录）
git sparse-checkout init --cone
# 按需指定 checkout 的内容（这里我们可以指定 checkout 哪些目录或文件）
git sparse-checkout set <dir1> <dir2>

git checkout
```

## 如何知道下载哪些代码就可以了？

那么如何确定下载哪些代码呢？

我们可以利用 PNPM Lockfile 来分析 Monorepo 子包的依赖关系：比如我们看到本仓库的 `pnpm-lock.yaml` 中 importers 下有列出来的包（及其依赖包），根据这些包（及其依赖包）我们获取到对应的 `package.json`，然后递归去分析某个包的依赖关系，获取到对应的目录，读取到目录/文件的依赖关系之后，我们就可以通过 `git sparse-checkout set` 设置需要检出的内容了。

另外一个思路是利用 PNPM List 来事先在本地把依赖关系存到根目录下的一个文件中，这样拉取代码时就不用现场分析依赖关系了。但是这就要求每次 packages 的依赖关系改变后，都来重新分析一遍依赖关系，这个我们可以通过在 `package.json` 中定义 `postinstall` 或 `postadd` 钩子来自动化处理。与实时分析依赖关系的方案相比，这个方案显然更优。

```bash
# 可以使用 pnpm ls 来递归获取到某个包的依赖关系
pnpm ls -r --json --filter <package_name>
```

## CI/CD

至此，我们已经知道单独部署某个包时需要下载哪部分代码，也知道如何只下载部分代码，剩下的就是 CI/CD 的事情了。

## Reference

- [部分克隆如何提升大仓库体验](https://help.aliyun.com/document_detail/309002.html)
- [git clone 仓库的部分代码](https://wayou.github.io/2019/09/27/git-clone-仓库的部分代码/)
- [Is it possible to do a sparse checkout without checking out the whole repository first?](https://stackoverflow.com/a/60729017)
- [Bring your monorepo down to size with sparse-checkout](https://github.blog/2020-01-17-bring-your-monorepo-down-to-size-with-sparse-checkout/)
- [Make your monorepo feel small with Git’s sparse index](https://github.blog/2021-11-10-make-your-monorepo-feel-small-with-gits-sparse-index/)
